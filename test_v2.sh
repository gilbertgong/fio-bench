#!/bin/sh

version="fio_v2.1"

# now uses fio instead of sysbench

time=1800
threads=16
depth=32
sleep=600
# ramp for 5 minutes before measuring
ramp=300
sizes="16 32 48 64 96 128 256 384"
work_dir="/home/ggong/work"
ioengine="posixaio"

# read in overrides if exists
if [ -f ".config" ]; then
  . ./.config
fi

ID=$1
if [ -z "$ID" ]; then
  echo no directory provided
  exit
fi

proceed=0
# check directory exists
if [ -d "${work_dir}/$ID" ]; then
  proceed=1
fi

if [ $proceed -eq 0 ]; then
  echo "directory $ID doesn't exit"
  exit
fi

init () {

  logfile=${work_dir}/log/log_${ID}.`date +%Y%m%d.%H%M`
  cd ${work_dir}/$ID

  echo "X_ID, $ID" | tee -a $logfile
  echo "X_LOG, $logfile" | tee -a $logfile
  echo "X_PARAMS, ioengine $ioengine time $time threads $threads, depth $depth, ramp $ramp" | tee -a $logfile
  echo "X_SIZES, $sizes" | tee -a $logfile
  echo "X_DIR, `pwd`" | tee -a $logfile
  echo "X_ARRAY, `cat array`" | tee -a $logfile
  echo "X_CLIENT, $version" | tee -a $logfile
}

set_cmd () {
  cmd="sysbench --test=fileio --file-rw-ratio=$ratio --file-fsync-end=off --file-fsync-freq=0 --file-total-size=$size --file-test-mode=$mode --max-time=$time --num-threads=$threads --report-interval=60 --max-requests=$req $action"
  cmd="fio --direct=1 --rw=$mode --size=$size --ioengine=$ioengine --bs=16k --rwmixread=$ratio --iodepth=$depth --numjobs=$threads --time_based --runtime=$time --ramp_time=$ramp --refill_buffers=1 --randrepeat=0 --sync=1 --group_reporting --eta=never --name=fio_test"

}

run () {
  echo "X_TEST: size $size_total, mode $mode, time $time" | tee -a $logfile
  action=run
  set_cmd
  date | tee -a $logfile
  echo "X_CMD: $cmd" | tee -a $logfile
  $cmd | tee -a $logfile
  date | tee -a $logfile
  sync
  sleep $sleep
}

prepare () {
  echo "deleting previous test files.."
  rm fio_test*
}

read () {
  mode=read
  ratio=100
  note="seq reads"
  do_sizes  
}

write () {
  mode=write
  ratio=0
  note="seq writes"
  do_sizes  
}

readwrite () {
  mode=readwrite
  ratio=70
  note="mixed seq ${ratio}% reads and writes"
  do_sizes  
}

randread () {
  mode=randread
  ratio=100
  note="random reads"
  do_sizes  
}

randwrite () {
  mode=randwrite
  ratio=0
  note="random writes"
  do_sizes  
}

randrw () {
  mode=randrw
  ratio=70
  note="mixed random ${ratio}% reads and writes"
  do_sizes
}

do_sizes () {
# mode and ratio should be set here
for i in $sizes; do

  size_total=$i
  # size is $i / threads
  s=`expr $i / $threads`
  size=${s}g

  prepare
  echo "X_STEP ${mode}, $note" | tee -a $outfile
  run
done
}

init
read
write
readwrite
randread
randwrite
randrw
