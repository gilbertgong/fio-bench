#!/usr/bin/perl

my $file = $ARGV[0];
# keep track of current mode
my $mode = "";
# keep track of current read and write IOPS numbers
my $read;
my $write;
my $debug=0;
my $sizes;
my $print_header=1;

sub d_print {
  $line = shift;
  if ($debug) {
    print ("DEBUG: $line");
  }
}

sub stat_header {
  if ($print_header) {
    print ("\nsize in GB,$sizes\n");
    $print_header=0;
  }
}

sub stat_print {
  my $mode = shift;
  my $stats = shift;

  if ($stats ne "") {
    chop $stats;
    print ("$mode,$stats\n");
    if ($mode =~ "write") {
      $print_header=1;
    }
  }
}

sub process_line {
  my $line = shift;

  if ($line =~ "X_TEST") {
    #do nothing
  } elsif ($line =~ "X_CMD") {
    ($t, $t, $t, $token) = split (/ /, $line);
#    print "token is $token\n";
    ($t, $new_mode) = split (/=/, $token);
#    print ("find a CMD line, mode currently $mode\n");
    if ($new_mode ne $mode) {
      # we have a new mode
      # so print out stats for old mode and then prep for new mode
      stat_header();
      stat_print ("${mode}_read", $read);
      stat_print ("${mode}_write", $write);
      $mode = $new_mode;
      $read = "";
      $write = "";
    }
  } elsif ($line =~ "X_") {
    print "$line\n";
    if ($line =~ "X_SIZES") {
      $sizes = $line;
      $sizes =~ s/X_SIZES: //;
      $sizes =~ s/ /,/g;
      d_print ("sizes: $sizes\n");
    }
    return;
  }

  if ($line =~ "iops") {
    ($t, $token) = split (/: /, $line);
    ($t, $t, $token) = split (/ /, $token);
    d_print ("token is $token\n");
    ($t, $iops) = split (/=/, $token);
    if ($line =~ "read") {
      $read .= $iops;
    } else {
      $write .= $iops;
    }
  }
}

open (my $fh, "<", $file)
  or die "Could not open file $file";

while (my $line = <$fh>) {
  chomp $line;
  process_line ($line);
}

# at the end, print out final stats      
stat_header();
stat_print ("${mode}_read", $read);
stat_print ("${mode}_write", $write);
