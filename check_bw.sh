#!/bin/sh

if=$1
sleep=10

echo Checking BW utilization for $if

old_rx_bytes=`ifconfig $if | grep bytes | awk '{print $2}' | awk -F ":" '{print $2}'`
old_tx_bytes=`ifconfig $if | grep bytes | awk '{print $6}' | awk -F ":" '{print $2}'`


while [ 1 ];do
  sleep $sleep
  rx_bytes=`ifconfig $if | grep bytes | awk '{print $2}' | awk -F ":" '{print $2}'`
  tx_bytes=`ifconfig $if | grep bytes | awk '{print $6}' | awk -F ":" '{print $2}'`
  rx_kbps=`echo "scale=2;($rx_bytes - $old_rx_bytes) * 8 / $sleep / 1000" | bc`
  rx_mbps=`echo "scale=2; $rx_kbps / 1000" | bc`
  tx_kbps=`echo "scale=2;($tx_bytes - $old_tx_bytes) * 8 / $sleep / 1000" | bc`
  tx_mbps=`echo "scale=2; $tx_kbps / 1000" | bc`
  echo "RX: $rx_mbps Mbps, TX: $tx_mbps Mbps (RX: $rx_kbps Kbps  TX: $tx_kbps Kbps)"

  old_rx_bytes=$rx_bytes
  old_tx_bytes=$tx_bytes
done
